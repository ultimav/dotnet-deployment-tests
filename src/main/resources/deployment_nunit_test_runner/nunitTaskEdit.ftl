[@ww.textfield labelKey='nunit.testDirectory' name='testResultsDirectory' required="true" cssClass="long-field" /]
[@ui.bambooSection titleKey='builder.common.tests.enabled' collapsible=true]
    [@ww.checkbox labelKey="builder.common.tests.pickup.outdated.files" name="pickupOutdatedFiles"/]
[/@ui.bambooSection]