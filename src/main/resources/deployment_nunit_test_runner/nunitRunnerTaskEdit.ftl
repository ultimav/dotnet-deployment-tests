[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='nunit' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='label'
            list=uiConfigSupport.getExecutableLabels('nunit')
            extraUtility=addExecutableLink required='true' /]

[@ww.textfield labelKey='nunit.testFiles' name='nunitTestFiles' required='true' cssClass="long-field" /]
[@ww.textfield labelKey='nunit.resultsFile' name='nunitResultsFile' required='true' cssClass="long-field" /]
[@ww.textfield labelKey='nunit.testsToRun' name='run' cssClass="long-field" /]
[@ww.textfield labelKey='nunit.includedCategories' name='include' cssClass="long-field" /]
[@ww.textfield labelKey='nunit.excludedCategories' name='exclude' cssClass="long-field" /]
[@ww.textfield labelKey='nunit.options' name='commandLineOptions' cssClass="long-field" /]
[@ww.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]

